# Smart Catbox Monitor

This project is an attempt to improve upon [Owen Ashurst's smart catbox monitor](https://hackaday.com/2023/08/17/litter-box-sensor-lets-you-know-exactly-what-the-cats-been-up-to/), but written in CircuitPython, running on a Raspberry Pi Pico W and using an Inland CCS811 sensor. The device senses the smell of a catbox, controlling an LED color to indicate how bad the smell is (for those who can't smell, or as a visual cue to check) and sending the status to an MQTT server. I recommend pairing this with Home Assistant and Mosquito Broker for sending alerts to your phone, email, or any other automation you want. I may be making an MQTT air freshener that pairs with this, though there's the issue that many air fresheners are picked up by my CCS811 that needs to be resolved somehow. 

## Features

1. **TVOC Sensing**: Detects the smell of the catbox and determines the color of the LED.
2. **MQTT Integration**: Sends all values, including the photosensor, to an MQTT server.
3. **Web Configurator**: Configure the MQTT server and TVOC levels.

## Hardware Requirements
- Raspberry Pi Pico W running CircuitPython (Or any wifi enabled rp2040 board, with modification)
- Inland CCS811 sensor
- NeoPixel
- Photoresistor
- Button for Reset pin, if your board lacks one

## Installation

1. Install CircuitPython on your Raspberry Pi Pico W following the instructions [here](https://learn.adafruit.com/welcome-to-circuitpython/installing-circuitpython-on-raspberry-pi-pico).
2. Connect the sensor, neopixel and photoresistor to the pins in main.py. Connect a button between pin 30 and 28 if you want a reset button. 
3. Copy `main.py`, `catbox.cfg`, `boot.py`, `index.htm`, `safemode.py` and the `lib` folder to your Raspberry Pi Pico W.
4. Delete `code.py`, if present, and set the WiFi settings in the `catbox.cfg` file.


## Usage
1. Power up the Raspberry Pi Pico W.
2. Access the web configurator through your web browser.
3. Configure the MQTT server settings and TVOC levels.
4. Place the device near the catbox to start monitoring.
5. Enjoy your Smart Catbox Monitor!
