import storage
import supervisor

print(supervisor.runtime.serial_connected)
if not supervisor.runtime.serial_connected:
    #print("\r\n\r\nNot connected to PC, storage is read-write!\r\n\r\n")
    storage.disable_usb_drive()
    storage.remount("/", False)
else:
    print("\r\n\r\nConnected to PC, storage is read-only!\r\n\r\n")