import board
import digitalio
import traceback
import gc

led = digitalio.DigitalInOut(board.LED)
led.direction = digitalio.Direction.OUTPUT
led.value = True

storage_rw = True
wifi_error = False
sensor_error = False
sensor_error_pc = False
mqtt_error = True
low_thr = 5
med_thr = 10
high_thr = 50
veryhigh_thr = 200
alert_thr = 300
alarm_thr = 500
neo_bright = 1
neo_bright_dim = 0.1
update_delay = 5
history_size = 16
wifi_ssid = ""
wifi_pass = ""
mqtt_host = ""
mqtt_port = 0
mqtt_user = ""
mqtt_pass = ""
server_user = "admin"
server_pass = "scent"

def load_settings():
	global server_user, server_pass, wifi_ssid, wifi_pass, mqtt_host, mqtt_port, mqtt_user, mqtt_pass, low_thr, med_thr, high_thr, veryhigh_thr, alert_thr, alarm_thr, neo_bright, neo_bright_dim, update_delay, history_size
	try:
		settings_file = open("/catbox.cfg", "r")
		settings_raw = settings_file.read()
		settings_file.close()
		settings_data = settings_raw.splitlines()
		print(settings_data)
		print(settings_data[9])
		low_thr = int(settings_data[0])
		med_thr = int(settings_data[1])
		high_thr = int(settings_data[2])
		veryhigh_thr = int(settings_data[3])
		alert_thr = int(settings_data[4])
		alarm_thr = int(settings_data[5])
		neo_bright = float(settings_data[6])
		neo_bright_dim = float(settings_data[7])
		update_delay = float(settings_data[8])
		history_size = int(settings_data[9])
		wifi_ssid = settings_data[10]
		wifi_pass = settings_data[11]
		mqtt_host = settings_data[12]
		mqtt_port = int(settings_data[13])
		mqtt_user = settings_data[14]
		mqtt_pass = settings_data[15]
		server_user = settings_data[16]
		server_pass = settings_data[17]
	except Exception as ex:
		error_details = traceback.format_exception(ex)
		print(f"\r\nLoad error! May be old file, in which case update and save new settings.\r\n{error_details}")
load_settings()



def save_settings():
	global server_user, server_pass, wifi_ssid, wifi_pass, mqtt_host, mqtt_port, mqtt_user, mqtt_pass,storage_rw, ui_storage_error, low_thr, med_thr, high_thr, veryhigh_thr, alert_thr, alarm_thr, neo_bright, update_delay , history_size
	if storage_rw:
		try:
			settings_file = open("/catbox.cfg", "w")
			settings_data = f"{low_thr}\n{med_thr}\n{high_thr}\n{veryhigh_thr}\n{alert_thr}\n{alarm_thr}\n{neo_bright}\n{neo_bright_dim}\n{update_delay}\n{history_size}\n{wifi_ssid}\n{wifi_pass}\n{mqtt_host}\n{mqtt_port}\n{mqtt_user}\n{mqtt_pass}\n{server_user}\n{server_pass}"
			settings_file.write(settings_data)
			settings_file.close()
		except:
			storage_rw = False
			ui_storage_error = "Connected to PC, storage is read-only!"
			print(ui_storage_error)
	else:
		print(ui_storage_error)

tvoc_history = [0] * history_size
current_index = 0
avg_tvoc = 0
ui_storage_error = ""

import neopixel
pixel_pin = board.GP16	# Change this to the pin your NeoPixels are connected to
num_pixels = 1
pixels = neopixel.NeoPixel(pixel_pin, num_pixels, brightness=neo_bright, auto_write=True, pixel_order=neopixel.RGB)

pixels.fill(0x0000FF)

from adafruit_led_animation.color import BLACK, TEAL, JADE, RED, BLUE, ORANGE, GREEN, YELLOW, ORANGE
import ipaddress
import ssl
import wifi
import socketpool
import adafruit_requests
import time
import asyncio
import sys
from adafruit_led_animation.animation.colorcycle import ColorCycle
from adafruit_debouncer import Debouncer
from adafruit_httpserver import Server, Request, Response, GET
from adafruit_httpserver.authentication import (
	AuthenticationError,
	Basic,
	Bearer,
	check_authentication,
	require_authentication,
)
import busio
import adafruit_ccs811
from collections import deque

colorcycle_wifi = ColorCycle(pixels, 0.5, colors=[BLUE, BLACK])
colorcycle_sensor = ColorCycle(pixels, 0.5, colors=[ORANGE, TEAL])
colorcycle_good = ColorCycle(pixels, 0.5, colors=[GREEN, 0x009900])
colorcycle_ok = ColorCycle(pixels, 0.5, colors=[GREEN, 0x999900])
colorcycle_poor = ColorCycle(pixels, 0.5, colors=[0x999900, 0x991100])
colorcycle_bad = ColorCycle(pixels, 0.5, colors=[RED, 0x990000])
colorcycle_verybad = ColorCycle(pixels, 0.5, colors=[RED, BLACK])
colorcycle_alert = ColorCycle(pixels, 0.2, colors=[RED, BLACK])
colorcycle_alarm = ColorCycle(pixels, 0.1, colors=[RED, BLACK])
colorcycle_wifi_error = ColorCycle(pixels, 0.2, colors=[RED, JADE])
colorcycle_sensor_error = ColorCycle(pixels, 0.2, colors=[RED, BLUE])
colorcycle_ha_error = ColorCycle(pixels, 0.2, colors=[RED, 0x990099])

neo_id = 7

pixels.fill(JADE)

pool = socketpool.SocketPool(wifi.radio)

#HTTP setup
server = Server(pool, "/static", debug=True)
# Create a list of available authentication methods.
auths = [
	Basic(server_user, server_pass),
]
server.require_authentication(auths)


def connect_wifi():
	global wifi_error, server
	try:
		pixels.fill(0x0000FF)
		time.sleep(0.5)
        wifi.radio.hostname("ScentSentry")
		#Setup Wifi
		print("My MAC addr:", [hex(i) for i in wifi.radio.mac_address])
		print("Connecting to %s"%wifi_ssid)
		wifi.radio.connect(wifi_ssid, wifi_pass)
		print("Connected to %s!"%wifi_ssid)
		time.sleep(1)
		print("My IP address is", wifi.radio.ipv4_address)
		pixels.fill(JADE)
		time.sleep(1)
		server.start(str(wifi.radio.ipv4_address))
		wifi_error = False
	except Exception as ex:
		error_details = traceback.format_exception(ex)
		print(f"\r\nWifi connect error: \r\n{error_details}")
		pixels.fill(0xFF0000)
		wifi_error = True

connect_wifi()

#MQTT setup
import ssl
import json
import alarm
import adafruit_minimqtt.adafruit_minimqtt as MQTT
import analogio

try:
	#sensor on 1 (SCL) and 0 (SDA)
	i2c = busio.I2C(board.GP1, board.GP0)
	ccs =  adafruit_ccs811.CCS811(i2c)
except Exception as ex:
	sensor_error = True

try:
	#Photocell sensor
	photocell = analogio.AnalogIn(board.GP28)
except Exception as ex:
	sensor_error_pc = True

def connect_sensor():
	global i2c, ccs, sensor_error, pixels
	try:
		pixels.fill(BLUE)
		#sensor on 1 (SCL) and 0 (SDA)
		i2c = busio.I2C(board.GP1, board.GP0)
		ccs =  adafruit_ccs811.CCS811(i2c)
		sensor_error = False
	except Exception as ex:
		error_details = traceback.format_exception(ex)
		print(f"\r\nMain Loop/Sensor Error!\r\n{error_details}")
		sensor_error = True

def parse_post_data(data):
	# Convert bytes to string and split key-value pairs
	pairs = data.decode().split('&')
	post_data = {}
	for pair in pairs:
		if '=' in pair:
			key, value = pair.split('=')
			post_data[key] = value
	return post_data

_hextobyte_cache = None

def unquote(string):
    """unquote('abc%20def') -> 'abc def'."""
    global _hextobyte_cache

    if not string:
        return ""

    if isinstance(string, str):
        string = string.encode('utf-8')

    bits = string.split(bytes('%', 'utf-8'))
    if len(bits) == 1:
        return string.decode('utf-8')

    res = [bits[0]]
    append = res.append

    if _hextobyte_cache is None:
        _hextobyte_cache = {}

    for item in bits[1:]:
        try:
            code = item[:2]
            char = _hextobyte_cache.get(code)
            if char is None:
                char = _hextobyte_cache[code] = bytes([int(str(code), 16)])
            append(char)
            append(item[2:])
        except KeyError:
            append(bytes('%', 'utf-8'))
            append(item)
    value = str(bytes(res).decode('utf-8'))
    print(value)
    return value  # Ensure you're returning a decoded string


@server.route("/")
def display_form(request: Request):
	global ui_storage_error
	try:
		html_content = f"""
<html><head><link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Sofia"><style> body {{ background-color: #2b2f3a; color: white; font-family: 'M PLUS 1 Code', 'Trebuchet MS', Arial, sans-serif; padding: 20px; }} div {{ border: 3px ridge #1F2230; border-radius: 12px; margin-bottom: 20px; padding: 10px; background-color: #34374a; }} table {{ width: 100%; border: 3px ridge #1F2230; border-radius: 12px; font-size: 1.3rem }} th, td {{ border: 3px ridge #1F2230; padding: 8px; text-align: left; }} th {{ font-weight: bold; }} .input-label {{ display: inline-block; width: 180px; padding-bottom: 0.4rem; padding-top: 0.4rem; font-weight: bold; margin: 0.5rem; background: linear-gradient(160deg, #FFFFFF, #87ceebCC, #00000000); color: black; z-index: 1; position: relative; }} input[type="text"], input[type="password"], input[type="number"] {{ width: 100%; padding: 10px; border: 3px ridge #1F2230; text-align: right; background-color: #555; color: white; border-radius: 10px; margin-bottom: 1rem; padding-top: 1rem; margin-top: -1.4rem; font-size: 1.5rem; }} input[type="submit"], button {{ width: 100%; padding: 10px; font-size: 1.3rem; font-weight: bold; border-radius: 15px; background: linear-gradient(160deg, #38ceff, #87ceeb); border: 3px ridge #FFF; cursor: pointer; transition: background-color 0.3s ease; }} input[type="submit"]:hover , button:hover{{ background: linear-gradient(160deg, #b2ecff, #225b73); }} h1 {{ font-family: 'Orbitron', 'Trebuchet MS', Arial, sans-serif; font-size: 26px; margin-bottom: 10px; color: #5efb81; border-bottom: 2px solid #555; display: inline-block; padding-bottom: 10px; }} h2 {{ font-size: 18px; margin-bottom: 20px; }} .column {{ width: 42%; float: left; margin-right: 2%; }} .column:last-child {{ margin-right: 0; }} small {{ font-size: 0.6rem; }} </style></head><body><h1 style="text-shadow: 2px 2px 4px #add8e6;">ScentSentry</h1>  <!-- Updated shadow here --><h2>A shameless clone of <a  style="color: green;" href="https://hackaday.com/2023/08/17/litter-box-sensor-lets-you-know-exactly-what-the-cats-been-up-to/">Owen Ashurst's smart catbox monitor</a>, but written in CircuitPython, running on a Raspberry Pi Pico W and using an Inland CCS811 sensor. UI Design by ChatGPT @ OpenAI.</h2><div><table><thead><tr><th>State</th><th>Average TVOC</th><th>CO2</th><th>Total Volatile Organic Compounds</th><th>Light Sensor<br><small>(0-65535)</small></th></tr></thead><tbody><tr><td>{smell_state}</td><td>{sum(tvoc_history) / len(tvoc_history)} ppb</td><td>{ccs.eco2} ppm</td><td>{ccs.tvoc} ppb</td><td>{photocell.value}</td></tr></tbody></table></div><div><form action="/update" method="post"><div class="column"><div class="input-label">Low:</div><input type="number" step="1" min="0" max="65535" name="low" value="{low_thr}"><br><div class="input-label">Medium:</div><input type="number" step="1" min="0" max="65535" name="medium" value="{med_thr}"><br><div class="input-label">High:</div><input type="number" step="1" min="0" max="65535" name="high" value="{high_thr}"><br><div class="input-label">Very High:</div><input type="number" step="1" min="0" max="65535" name="veryhigh" value="{veryhigh_thr}"><br><div class="input-label">Alert:</div><input type="number" step="1" min="0" max="65535" name="alert" value="{alert_thr}"><br><div class="input-label">Alarm:</div><input type="number" step="1" min="0" max="65535" name="alarm" value="{alarm_thr}"><br></div><div class="column"><div class="input-label">Neopixel Brightness:</div><input type="number" step="any" min="0.01" max="1" name="brightness" value="{neo_bright}"><br><div class="input-label">Neopixel Dim Brightness:</div><input type="number" step="any" min="0.01" max="1" name="brightness_dim" value="{neo_bright_dim}"><br><div class="input-label">Update Delay:</div><input type="number" step="any" name="delay" min="0" max="6000" value="{update_delay}"><br><div class="input-label">History Size:</div><input type="number" step="1" min="2" max="50" name="history" value="{history_size}"><br></div><div class="column"><div class="input-label">Wifi SSID:</div><input type="text" name="wifi-ssid" value="{wifi_ssid}"><br><div class="input-label">Wifi Password:</div><input type="password" name="wifi-pwd" value="{wifi_pass}"><br></div><div class="column"><div class="input-label">MQTT Host:</div><input type="text" name="mqtt-host" value="{mqtt_host}"><br><div class="input-label">MQTT Port:</div><input type="number" step="1" min="1" max="65535" name="mqtt-port" value="{mqtt_port}"><br><div class="input-label">MQTT User:</div><input type="text" name="mqtt-user" value="{mqtt_user}"><br><div class="input-label">MQTT Password:</div><input type="password" name="mqtt-pass" value="{mqtt_pass}"><br></div><div class="column"><div class="input-label">Device User:</div><input type="text" name="server-user" value="{server_user}"><br><div class="input-label">Device Password:</div><input type="password" name="server-pass" value="{server_pass}"><br></div><div class="column"><br><a style="color: red">{ui_storage_error}</a><br><br><input type="submit" value="Update"><button onclick="location.href='/reset'" type="button">Reset</button><button onclick="location.href='/reboot'" type="button">Reboot</button></div><div style="clear: both; padding-top: 20px;"></div></form></div></body></html>
	   """

		headers = {"Content-Type": "text/html"}
		return Response(request, html_content, headers=headers)

	except Exception as ex:
		error_details = traceback.format_exception(ex)
		return Response(request, f"An error occurred: {ex}\n\nDetails:\n{error_details}")

@server.route("/reset")
def reload_cpy(request: Request):
	global ui_storage_error
	import supervisor
	supervisor.reload()
    
@server.route("/reboot")
def reload_cpy(request: Request):
	global ui_storage_error
	import microcontroller
	microcontroller.reset()
    
@server.route("/update", methods=["POST"])
def update_values(request: Request):
	global server_user, server_pass, pixels, wifi_ssid, wifi_pass, mqtt_host, mqtt_port, mqtt_user, mqtt_pass,tvoc_history, history_size, low_thr, med_thr, high_thr, veryhigh_thr, alert_thr, alarm_thr, neo_bright, update_delay
	try:
		post_data = parse_post_data(request.body)

		if "low" in post_data and post_data["low"].isdigit():
			try:
				low_thr = int(post_data["low"])
			except ValueError:
				pass  # if the conversion fails, do nothing (keep the original value)

		if "medium" in post_data and post_data["medium"].isdigit():
			try:
				med_thr = int(post_data["medium"])
			except ValueError:
				pass

		if "high" in post_data and post_data["high"].isdigit():
			try:
				high_thr = int(post_data["high"])
			except ValueError:
				pass

		if "veryhigh" in post_data and post_data["veryhigh"].isdigit():
			try:
				veryhigh_thr = int(post_data["veryhigh"])
			except ValueError:
				pass

		if "alert" in post_data and post_data["alert"].isdigit():
			try:
				alert_thr = int(post_data["alert"])
			except ValueError:
				pass

		if "alarm" in post_data and post_data["alarm"].isdigit():
			try:
				alarm_thr = int(post_data["alarm"])
			except ValueError:
				pass

		if "brightness" in post_data and post_data["brightness"].isdigit():
			try:
				neo_bright = int(post_data["brightness"])
			except ValueError:
				pass

		if "brightness_dim" in post_data and post_data["brightness_dim"].isdigit():
			try:
				neo_bright_dim = int(post_data["brightness_dim"])
			except ValueError:
				pass

		if "history" in post_data and post_data["history"].isdigit():
			try:
				history = int(post_data["history"])
			except ValueError:
				pass

		if "delay" in post_data and post_data["delay"].isdigit():
			try:
				update_delay = int(post_data["delay"])
			except ValueError:
				pass

		if "wifi-ssid" in post_data:
			try:
				wifi_ssid = unquote(post_data["wifi-ssid"])
			except ValueError:
				pass

		if "wifi-pwd" in post_data:
			try:
				wifi_pass = unquote(post_data["wifi-pwd"])
			except ValueError:
				pass

		if "mqtt-host" in post_data:
			try:
				mqtt_host = unquote(post_data["mqtt-host"])
			except ValueError:
				pass

		if "mqtt-port" in post_data and post_data["mqtt-port"].isdigit():
			try:
				mqtt_port = int(post_data["mqtt-port"])
			except ValueError:
				pass

		if "mqtt-user" in post_data:
			try:
				mqtt_user = unquote(post_data["mqtt-user"])
			except ValueError:
				pass

		if "mqtt-pass" in post_data:
			try:
				mqtt_pass = unquote(post_data["mqtt-pass"])
			except ValueError:
				pass

		if "server-user" in post_data:
			try:
				server_user = unquote(post_data["server-user"])
			except ValueError:
				pass

		if "server-pass" in post_data:
			try:
				server_pass = unquote(post_data["server-pass"])
			except ValueError:
				pass
		# To handle floats, we use a regex pattern to match valid float and integer inputs
		import re

		float_pattern = r"^\d+(\.\d+)?$"

		if "brightness" in post_data and re.match(float_pattern, post_data["brightness"]):
			try:
				brightness = float(post_data["brightness"])
			except ValueError:
				pass

		if "delay" in post_data and re.match(float_pattern, post_data["delay"]):
			try:
				delay = float(post_data["delay"])
			except ValueError:
				pass

		if history != history_size:
			print(f"history changing from {history_size} to {history}")
			history_size = history
			tvoc_history = None
			tvoc_history = [0] * history_size
			print(f"Results: {history_size} {tvoc_history}")

		save_settings()
		return display_form(request)

	except Exception as ex:
		error_details = traceback.format_exception(ex)
		return Response(request, f"An error occurred: {ex}\n\nDetails:\n{error_details}")

async def main_loop():
	global ccs, history_size, tvoc_history, smell_state, avg_tvoc, current_index, neo_id, update_delay, low_thr, med_thr, high_thr, veryhigh_thr, alert_thr, alarm_thr, sensor_error

	while True:
		try:
			sensor_error = False
			print("CO2: ", ccs.eco2, " TVOC:", ccs.tvoc)

			# Replace the oldest ccs.tvoc reading in the list with the new one
			if ccs.tvoc is not None:
				tvoc_history[current_index] = ccs.tvoc

			# Update the index for the next iteration
			current_index = (current_index + 1) % history_size

			# Calculate the average TVOC
			avg_tvoc = sum(tvoc_history) / len(tvoc_history)
			print("avg_tvoc: ", avg_tvoc, " Len:", len(tvoc_history))

			if avg_tvoc	 < low_thr:
				smell_state = "Good"
				neo_id = 0
			elif avg_tvoc  < med_thr:
				smell_state = "Low"
				neo_id = 1
			elif avg_tvoc  < high_thr:
				smell_state = "Medium"
				neo_id = 2
			elif avg_tvoc  < veryhigh_thr:
				smell_state = "High"
				neo_id = 3
			elif  avg_tvoc	< alert_thr:
				smell_state = "Very High"
				neo_id = 4
			elif  avg_tvoc	< alarm_thr:
				smell_state = "Alert"
				neo_id = 5
			else:
				smell_state = "Alarm!"
				neo_id = 6

			gc.collect()
			await asyncio.sleep(update_delay)
		except Exception as ex:
			error_details = traceback.format_exception(ex)
			print(f"\r\nMain Loop/Sensor Error!\r\n{error_details}")
			sensor_error = True
			connect_sensor()
			neo_id = 7
			await asyncio.sleep(1)

async def neopixel_loop():
		global pixels, photocell, neo_id, colorcycle_good, wifi_error, sensor_error
		while True:
			if wifi_error:
				colorcycle_wifi.animate()
			elif sensor_error:
				colorcycle_sensor.animate()
			elif neo_id == 0:
				colorcycle_good.animate()
			elif neo_id == 1:
				colorcycle_ok.animate()
			elif neo_id == 2:
				colorcycle_poor.animate()
			elif neo_id == 3:
				colorcycle_bad.animate()
			elif neo_id == 4:
				colorcycle_verybad.animate()
			elif neo_id == 5:
				colorcycle_alert.animate()
			elif neo_id == 6:
				colorcycle_alarm.animate()
			elif neo_id == 7:
				colorcycle_sensor_error.animate()
			elif neo_id == 8:
				colorcycle_wifi_error.animate()
			elif neo_id == 9:
				colorcycle_ha_error.animate()

			#photocell
			#print(photocell.value)
			if (photocell.value > 16000):
				pixels.brightness = neo_bright
			else:
				pixels.brightness = neo_bright_dim

			await asyncio.sleep(0.05)

async def wifi_loop():
	global wifi_error, neo_id
	while True:
		if wifi_error:
			neo_id = 8
			connect_wifi()
			await asyncio.sleep(3)
		else:
			try:
				server.poll()
				await asyncio.sleep(0.05)
			except Exception as ex:
				error_details = traceback.format_exception(ex)
				print(f"Wifi error...reconnecting... {error_details}")
				wifi_error = True
				await asyncio.sleep(5)

async def mqtt_loop():
	global ccs, photocell, smell_state, avg_tvoc, mqtt_error, mqtt_client, neo_id, pixels, pool
	pixels.fill(0xFF00FF)
	mqtt_client = MQTT.MQTT(
			broker=mqtt_host,
			port=mqtt_port,
			username=mqtt_user,
			password=mqtt_pass,
			socket_pool=pool,
			#ssl_context=ssl.create_default_context(),
		)

	MQTT_TOPIC = "state/ScentSentry"

	while True:
		try:
			if mqtt_error:
				print("Attempting to connect to %s" % mqtt_client.broker)
				mqtt_client.connect()
				mqtt_error = False
				await asyncio.sleep(3)
			else:
				#char_array_state = list(smell_state)

				print("Publishing to %s" % MQTT_TOPIC)
				output = {
					"TVOC": ccs.tvoc,
					"CO2": ccs.eco2,
					"AVG_TVOC": avg_tvoc,
					"STATE": smell_state,
					"PHOTOCELL": photocell.value
				}
				mqtt_client.publish(MQTT_TOPIC, json.dumps(output))
				await asyncio.sleep(update_delay)
		except Exception as ex:
			error_details = traceback.format_exception(ex)
			print(f"MQTT error...reconnecting... {error_details}")
			mqtt_error = True
			neo_id = 9
			await asyncio.sleep(3)

async def main():
	main_task = asyncio.create_task(main_loop())
	neopixel_task = asyncio.create_task(neopixel_loop())
	wifi_task = asyncio.create_task(wifi_loop())
	mqtt_task = asyncio.create_task(mqtt_loop())
	await asyncio.gather(main_task,wifi_task,neopixel_task,mqtt_task)

asyncio.run(main())